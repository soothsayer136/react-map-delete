import React, { Component } from 'react';
import DisplayComponent from './DisplayComponents'

class MapComponents extends Component {

    state = {
       myArray: [{
            id:1,
            name:'bikram',
            email:'bikram@gmail.com',
            phone:988888888
        },
        {
            id:2,
            name:'rakesh',
            email:'rakesh@gmail.com',
            phone:981111111
        }, 
        {
            id:3,
            name:'sandis',
            email:'sandis@gmail.com',
            phone:987777777
        },
        {
            id:4,
            name:'bhuwan',
            email:'bhuwan@gmail.com',
            phone:982222222    
        }
    ]
    }
    

    handleDelete =(id) => {
  const myArray = this.state.myArray.filter(item => item.id !==id)
  this.setState({myArray:myArray})
       
    }

    showData() {
        return <DisplayComponent
        deleteItem = { this.handleDelete.bind(this)}
         myArray={this.state.myArray} />
    }

    render() {
        return (
            <div>
                <h1>Map Component</h1>
                {this.showData()}
            </div>
        );
    }
}

export default MapComponents;
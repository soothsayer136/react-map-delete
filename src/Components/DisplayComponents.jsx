import React, { Component } from 'react';
import TableComponent from './TableComponent';

class DisplayComponents extends Component {

    handleItem(name){
        this.props.deleteItem(name)
    }


    render() {
        return (
            <div>
                <table style={{width:500}}>
                    <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                    </thead>
                    <tbody>
                        {this.props.myArray.map((item, index) =>{
                            return <TableComponent 
                            deleteItem ={this.handleItem.bind(this)}
                            key= {index}itemElements={item}/>
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default DisplayComponents;
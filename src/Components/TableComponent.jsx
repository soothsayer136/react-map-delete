import React, { Component } from 'react';

class TableComponent extends Component {

    deleteRow(id) {
        this.props.deleteItem(id)
    }

    render() {
        return (
            
                <tr>
                    <td>{this.props.itemElements.name}</td>
                    <td>{this.props.itemElements.email}</td>
                    <td>{this.props.itemElements.phone}</td>
                    <td><button style={{height:30}} onClick ={() => this.deleteRow(this.props.itemElements.id)}>Delete</button></td>
                </tr>
           
        );
    }
}

export default TableComponent;

import './App.css';
import MapComponent from './Components/MapComponents'

function App() {
  return (
    <div>
      <MapComponent />
    </div>
  );
}

export default App;
